competition: Test-Comp
year: 2023

verifiers:
  cmaesfuzz:
    name: CMA-ES Fuzz
    lang: C
    url: https://github.com/lazygrey/fuzzing_with_cmaes
    required-ubuntu-packages: []
    jury-member:
      name: Hors Concours
      institution: --
      country: --
      url:
    code-owner: gernst
  coveritest:
    name: CoVeriTest
    lang: C
    url: https://cpachecker.sosy-lab.org/
    required-ubuntu-packages:
      - openjdk-11-jre-headless
    jury-member:
      name: Marie-Christine Jakobs
      institution: TU Darmstadt
      country: Germany
      url: 
    code-owner: mjakobs
  FuSeBMC:
    name: FuSeBMC
    lang: C
    url: https://github.com/kaled-alshmrany/FuSeBMC
    required-ubuntu-packages: []
    jury-member:
      name: Kaled Alshmrany
      institution: University of Manchester / Institute of Public Administration
      country: UK / Saudi Arabia
      url: https://www.research.manchester.ac.uk/portal/kaled.alshmrany-postgrad.html
    code-owner: kaled-alshmrany
  hybridtiger:
    name: HybridTiger
    lang: C
    url: https://www.es.tu-darmstadt.de/es/team/sebastian-ruland/testcomp20
    required-ubuntu-packages: []
    jury-member:
      name: Hors Concours
      institution: --
      country: --
      url: 
    code-owner: sruland
  klee:
    name: KLEE
    lang: C
    url: https://klee.github.io
    required-ubuntu-packages:
      - clang-9
      - clang-10
      - llvm-9
      - llvm-10
    jury-member:
      name: Hors Concours
      institution: --
      country: --
      url:
    code-owner: mnowack
  legion:
    name: Legion
    lang: C
    url: https://github.com/Alan32Liu/Legion/tree/TestComp2020-ASE2020v3
    required-ubuntu-packages: []
    jury-member:
      name: Gidon Ernst
      institution: LMU Munich
      country: Germany
      url: https://www.sosy-lab.org/people/ernst/
    code-owner: gernst
  legion-symcc:
    name: Legion/SymCC
    lang: C
    url: https://github.com/gernst/legion-symcc
    required-ubuntu-packages: []
    jury-member:
      name: Gidon Ernst
      institution: LMU Munich
      country: Germany
      url: https://www.sosy-lab.org/people/ernst/
    code-owner: gernst
  libkluzzer:
    name: LibKluzzer
    lang: C
    url: https://unihb.eu/kluzzer
    required-ubuntu-packages:
      - clang
      - clang-6.0
      - g++-multilib
      - llvm
      - llvm-6.0
      - python3
    jury-member:
      name: Hoang M. Le
      institution: University of Bremen
      country: Germany
      url: 
    code-owner: hoangmle
  prtest:
    name: PRTest
    lang: C
    url: https://gitlab.com/sosy-lab/software/prtest
    required-ubuntu-packages: []
    jury-member:
      name: Thomas Lemberger
      institution: QAware GmbH
      country: Germany
      url: https://thomaslemberger.com/
    code-owner: lemberger
  symbiotic:
    name: Symbiotic
    lang: C
    url: https://github.com/staticafi/symbiotic
    required-ubuntu-packages:
      - python3
      - python3-lxml
    jury-member:
      name: Marek Chalupa
      institution: Masaryk University, Brno
      country: Czechia
      url: 
    code-owner: mchalupa
  tracerx:
    name: TracerX
    lang: C
    url: https://tracer-x.github.io/
    required-ubuntu-packages: []
    jury-member:
      name: Joxan Jaffar
      institution: National University of Singapore
      country: Singapore
      url: https://www.comp.nus.edu.sg/cs/bio/joxan/
    code-owner:
      - sajjadrsm
      - sanghu1790
  verifuzz:
    name: VeriFuzz
    lang: C
    url:
    required-ubuntu-packages:
      - gcc-multilib
      - libc6-dev-i386
      - python2
      - python3-sklearn
      - python3-pandas
    jury-member:
      name: Raveendra Kumar Medicherla
      institution: Tata Consultancy Services
      country: India
      url: 
    code-owner: raveenkm
  wasp-c:
    name: WASP-C
    lang: C
    url: https://github.com/wasp-platform/wasp
    required-ubuntu-packages:
      - gcc
      - make
      - python3
      - python3-lxml
    jury-member:
      name: Filipe Marques
      institution: INESC-ID, Lisbon
      country: Portugal
      url: https://filipeom.github.io/
    code-owner:
      - filipeom
      - j3fsantos

validators:
  testcov-validate-test-suites:
    name: TestCov
    lang: C
    url: https://gitlab.com/sosy-lab/software/test-suite-validator/
    required-ubuntu-packages:
      - clang-tidy
      - lcov
    jury-member:
      name: Matthias Kettl
      institution: LMU Munich
      country: Germany
      url: https://www.sosy-lab.org/people/kettl/
    code-owner: matthiaskettl

# Verifiers participating hors concours are executed and listed in the final results,
# but can not win any medals
hors_concours:
  - hybridtiger
  - klee

not_participating:
  - esbmc-kind
  - esbmc-falsi
  - fairfuzz

properties:
  - coverage-branches
  - coverage-error-call

categories:
  Cover-Error:
    properties: coverage-error-call
    categories:
      - coverage-error-call.ReachSafety-Arrays
      - coverage-error-call.ReachSafety-BitVectors
      - coverage-error-call.ReachSafety-ControlFlow
      - coverage-error-call.ReachSafety-ECA
      - coverage-error-call.ReachSafety-Floats
      - coverage-error-call.ReachSafety-Heap
      - coverage-error-call.ReachSafety-Loops
      - coverage-error-call.ReachSafety-ProductLines
      - coverage-error-call.ReachSafety-Recursive
      - coverage-error-call.ReachSafety-Sequentialized
      - coverage-error-call.ReachSafety-XCSP
      - coverage-error-call.ReachSafety-Hardware
      - coverage-error-call.SoftwareSystems-BusyBox-MemSafety
      - coverage-error-call.SoftwareSystems-DeviceDriversLinux64-ReachSafety
    verifiers:
      - cmaesfuzz
      - coveritest
      - FuSeBMC
      - hybridtiger
      - klee
      - legion
      - libkluzzer
      - prtest
      - symbiotic
      - tracerx
      - verifuzz
      - wasp-c
  Cover-Branches:
    properties: coverage-branches
    categories:
      - coverage-branches.ReachSafety-Arrays
      - coverage-branches.ReachSafety-BitVectors
      - coverage-branches.ReachSafety-ControlFlow
      - coverage-branches.ReachSafety-ECA
      - coverage-branches.ReachSafety-Floats
      - coverage-branches.ReachSafety-Heap
      - coverage-branches.ReachSafety-Loops
      - coverage-branches.ReachSafety-ProductLines
      - coverage-branches.ReachSafety-Recursive
      - coverage-branches.ReachSafety-Sequentialized
      - coverage-branches.ReachSafety-XCSP
      - coverage-branches.ReachSafety-Combinations
      - coverage-branches.SoftwareSystems-BusyBox-MemSafety
      - coverage-branches.SoftwareSystems-DeviceDriversLinux64-ReachSafety
      - coverage-branches.SoftwareSystems-SQLite-MemSafety
      - coverage-branches.Termination-MainHeap
    verifiers:
      - cmaesfuzz
      - coveritest
      - FuSeBMC
      - hybridtiger
      - klee
      - legion
      - legion-symcc
      - libkluzzer
      - prtest
      - symbiotic
      - tracerx
      - verifuzz
      - wasp-c

  Overall:
    properties:
      - coverage-branches
      - coverage-error-call
    categories:
      - Cover-Error
      - Cover-Branches
    verifiers:
      - cmaesfuzz
      - coveritest
      - FuSeBMC
      - hybridtiger
      - klee
      - legion
      - libkluzzer
      - prtest
      - symbiotic
      - tracerx
      - verifuzz
      - wasp-c


categories_process_order:
  - coverage-error-call.ReachSafety-Arrays
  - coverage-error-call.ReachSafety-BitVectors
  - coverage-error-call.ReachSafety-ControlFlow
  - coverage-error-call.ReachSafety-ECA
  - coverage-error-call.ReachSafety-Floats
  - coverage-error-call.ReachSafety-Heap
  - coverage-error-call.ReachSafety-Loops
  - coverage-error-call.ReachSafety-ProductLines
  - coverage-error-call.ReachSafety-Recursive
  - coverage-error-call.ReachSafety-Sequentialized
  - coverage-error-call.ReachSafety-XCSP
  - coverage-error-call.ReachSafety-Hardware
  - coverage-error-call.SoftwareSystems-BusyBox-MemSafety
  - coverage-error-call.SoftwareSystems-DeviceDriversLinux64-ReachSafety
  - Cover-Error
  - coverage-branches.ReachSafety-Arrays
  - coverage-branches.ReachSafety-BitVectors
  - coverage-branches.ReachSafety-ControlFlow
  - coverage-branches.ReachSafety-ECA
  - coverage-branches.ReachSafety-Floats
  - coverage-branches.ReachSafety-Heap
  - coverage-branches.ReachSafety-Loops
  - coverage-branches.ReachSafety-ProductLines
  - coverage-branches.ReachSafety-Recursive
  - coverage-branches.ReachSafety-Sequentialized
  - coverage-branches.ReachSafety-XCSP
  - coverage-branches.ReachSafety-Combinations
  - coverage-branches.SoftwareSystems-BusyBox-MemSafety
  - coverage-branches.SoftwareSystems-DeviceDriversLinux64-ReachSafety
  - coverage-branches.SoftwareSystems-SQLite-MemSafety
  - coverage-branches.Termination-MainHeap
  - Cover-Branches
  - Overall

categories_table_order:
  - Cover-Error
  - coverage-error-call.ReachSafety-Arrays
  - coverage-error-call.ReachSafety-BitVectors
  - coverage-error-call.ReachSafety-ControlFlow
  - coverage-error-call.ReachSafety-ECA
  - coverage-error-call.ReachSafety-Floats
  - coverage-error-call.ReachSafety-Heap
  - coverage-error-call.ReachSafety-Loops
  - coverage-error-call.ReachSafety-ProductLines
  - coverage-error-call.ReachSafety-Recursive
  - coverage-error-call.ReachSafety-Sequentialized
  - coverage-error-call.ReachSafety-XCSP
  - coverage-error-call.ReachSafety-Hardware
  - coverage-error-call.SoftwareSystems-BusyBox-MemSafety
  - coverage-error-call.SoftwareSystems-DeviceDriversLinux64-ReachSafety
  - Cover-Branches
  - coverage-branches.ReachSafety-Arrays
  - coverage-branches.ReachSafety-BitVectors
  - coverage-branches.ReachSafety-ControlFlow
  - coverage-branches.ReachSafety-ECA
  - coverage-branches.ReachSafety-Floats
  - coverage-branches.ReachSafety-Heap
  - coverage-branches.ReachSafety-Loops
  - coverage-branches.ReachSafety-ProductLines
  - coverage-branches.ReachSafety-Recursive
  - coverage-branches.ReachSafety-Sequentialized
  - coverage-branches.ReachSafety-XCSP
  - coverage-branches.ReachSafety-Combinations
  - coverage-branches.SoftwareSystems-BusyBox-MemSafety
  - coverage-branches.SoftwareSystems-DeviceDriversLinux64-ReachSafety
  - coverage-branches.SoftwareSystems-SQLite-MemSafety
  - coverage-branches.Termination-MainHeap
  - Overall

opt_out: 

opt_in: 

